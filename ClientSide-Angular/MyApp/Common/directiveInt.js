﻿
    myApp.directive("interger", function ($filter, $browser) {
        var result = {
            restrict: "A",
            require : "ngModel",
            link: function (scope, elemt, atrr, ctr) {
                var listener = function () {
                    var value = elemt.val().replace(/,/g, '');
                    elemt.val($filter("number")(value, false));
                }
                ctr.$parsers.push(function(viewValue){
                   return viewValue.replace(/,/g,'');
                });
                ctr.$render = function () {
                    elemt.val($filter("number")(ctr.$viewValue, false));
                };
                elemt.bind("change", listener);
                elemt.bind("keydown", function (event) {
                    var key = event.keycode;
                    if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40))
                        return
                    $browser.defer(listener);
                });
                //elemt.bind("paste cut", function () {
                //    $browser.defer(listener);
                //});
            }
        };
        return result;
    }
        );
