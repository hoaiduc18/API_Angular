﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerSide_API.Models.Response
{
    public class EmployeeDtoResponse
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string InfomationEmployee
        {
            get
            {
                return string.Format("{0}-{1}-{2}", ID, FullName, Address);
            }
        }
    }
}