﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerSide_API.Models.Request
{
    public class InsertEmployeeRequest
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
    }
}