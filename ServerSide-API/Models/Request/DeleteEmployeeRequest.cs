﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerSide_API.Models.Request
{
    public class DeleteEmployeeRequest
    {
        public int ID { get; set; }
    }
}