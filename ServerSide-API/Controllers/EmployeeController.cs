﻿using DataAccessLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ServerSide_API.Common;
using ServerSide_API.Models;
using ServerSide_API.Models.Request;
using ServerSide_API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ServerSide_API.Controllers
{
    [RoutePrefix("employee")]
    public class EmployeeController : ApiController
    {
        public static List<string> location = new List<string>();
        public static JsonSerializerSettings jsonView = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        [HttpGet]
        [Route("info")]
        public IHttpActionResult GetUrl([FromUri] string pass)
        {
            if (string.IsNullOrEmpty(pass))
                return Ok(new ArgumentNullException());
            if (pass.Equals("api"))
                return Ok(location.Distinct());
            var k = new Exception("Not allow access data");
            return Json<Object>( new { FullName = "Trung Quan" }, jsonView);
        }
        [HttpGet]
        [Route("getallemployee")]
        public IHttpActionResult GetAllEmployee()
        {
            //var k = new ArgumentNullException("cvkv");
            //var ko = Encoding.Unicode;
            location.Add(HttpContext.Current.Request.Url.AbsoluteUri);
            using (ServerClientDbEntities dbContext = new ServerClientDbEntities())
            {
                var result = dbContext.SC_Employee.Select(x => new EmployeeDtoResponse
                {
                    ID = x.ID,
                    FullName = x.FullName,
                    Address = x.Address
                }).ToList();
                //return Json<List<EmployeeDtoResponse>>(result, new JsonSerializerSettings (), Encoding.ASCII);
                return Json(JSON_Result.Push(HttpStatusCode.OK, result), jsonView);
                //return await ExecuteAsync(new HttpControllerContext(), new CancellationToken());
            }

        }

        [Route("getemployee")]
        [HttpGet]
        public IHttpActionResult GetEmployeebyID([FromUri] int employeeID)
        {
            var employeeRequest = new EmployeeResquest
            {
                ID = employeeID
            };
            location.Add(HttpContext.Current.Request.Url.AbsoluteUri);
            using (ServerClientDbEntities dbContext = new ServerClientDbEntities())
            {
                if (employeeRequest.ID == 0) return Ok();
                if (employeeRequest.ID < 0) return Ok();
                var response = dbContext.SC_Employee.FirstOrDefault(x => x.ID == employeeRequest.ID);
                if (response == null) return Ok();
                var employeeDtoResponse = new EmployeeDtoResponse
                {
                    ID = response.ID,
                    FullName = response.FullName,
                    Address = response.Address
                };
                return Ok(new { status = HttpStatusCode.OK, content = employeeDtoResponse, localtion = HttpContext.Current.Request.Url.AbsoluteUri });
            }
        }
        [HttpPost]
        [Route("insertemployee")]
        public IHttpActionResult InsetEmployee([FromBody] InsertEmployeeRequest request)
        {
            using (ServerClientDbEntities dbContext = new ServerClientDbEntities())
            {
                var newEmployee = new SC_Employee
                {
                    ID = request.ID,
                    FullName = request.FullName,
                    Address = request.Address
                };
                dbContext.SC_Employee.Add(newEmployee);
                dbContext.SaveChanges();
                return Ok();
            }
        }
        [HttpPut]
        [Route("updateemployee")]
        public IHttpActionResult UpdateEmployee([FromUri] int ID, [FromBody] InsertEmployeeRequest request)
        {
            using (ServerClientDbEntities dbContext = new ServerClientDbEntities())
            {
                var response = dbContext.SC_Employee.FirstOrDefault(x => x.ID == ID);
                if (response == null) return Ok(HttpStatusCode.OK);
                response.FullName = request.FullName;
                dbContext.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
        }
        [HttpDelete]
        [Route("deleteemployee")]
        public IHttpActionResult DeleteEmployee([FromUri]int ID)
        {
            var deleteEmployeeRequest = new DeleteEmployeeRequest
            {
                ID = ID
            };
            using (ServerClientDbEntities dbContext = new ServerClientDbEntities())
            {
                if (deleteEmployeeRequest.ID == 0) return Ok(HttpStatusCode.NotImplemented);
                if (deleteEmployeeRequest.ID < 0) return Ok(HttpStatusCode.NotImplemented);
                var response = dbContext.SC_Employee.FirstOrDefault(x => x.ID == deleteEmployeeRequest.ID);
                if (response == null) return Ok(HttpStatusCode.NotImplemented);
                dbContext.SC_Employee.Remove(response);
                dbContext.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
        }
    }
}
