﻿using Newtonsoft.Json.Serialization;
using ServerSide_API.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ServerSide_API.Common
{
    public class JSON_Result : IHttpActionResult
    {
        public HttpStatusCode StatusCode { get; set; }

        public Object Content { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                HttpResponseMessage response;

                var formatJson = new JsonMediaTypeFormatter
                {
                    SerializerSettings =
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }
                };
                response = new HttpResponseMessage
                {
                    Content = new ObjectContent<object>(Content, formatJson),
                    StatusCode = StatusCode,
                };
                //response.Headers.TransferEncodingChunked = Chunked;
                //response.Headers.CacheControl = CacheControlHeader;
                //response.Headers.ETag = ETag;
                return response;
            }, cancellationToken);
        }
        public static JSON_Result Push(HttpStatusCode statusCode, Object _content)
        {
            return new JSON_Result
            {
                Content = _content,
                StatusCode = statusCode
            };
        }
    }
}