﻿using Cadena.Library;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cadena.WebApi.Common
{
    public class PlainTextResult : IHttpActionResult
    {
        public PlainTextResult()
        {
            StatusCode = HttpStatusCode.OK;
            Content = string.Empty;
        }

        public PlainTextResult(HttpStatusCode statusCode, string content)
            : this()
        {
            StatusCode = statusCode;
            Content = content;
        }

        public PlainTextResult(string content)
            : this()
        {
            Content = content;
            StatusCode = HttpStatusCode.OK;
        }

        public PlainTextResult(HttpStatusCode statusCode)
            : this()
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; set; }

        public string Content { get; set; }

        public bool Chunked { get; set; }

        public CacheControlHeaderValue CacheControlHeader { get; set; }

        public EntityTagHeaderValue ETag { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var response = new HttpResponseMessage
                {
                    Content = new StringContent(Content, Encoding.UTF8, "application/text"),
                    StatusCode = StatusCode
                };
                response.Headers.CacheControl = CacheControlHeader;
                response.Headers.ETag = ETag;
                return response;
            }, cancellationToken);
        }

        private static EntityTagHeaderValue CreateTag(string content)
        {
            var hashedContent = content.GetMd5();
            var hashedEtag = string.Format("\"{0}\"", hashedContent);
            var eTag = new EntityTagHeaderValue(hashedEtag);
            return eTag;
        }

        private static EntityTagHeaderValue CreateTag(object content)
        {
            var hashedContent = content.GetString().GetMd5();
            var hashedEtag = string.Format("\"{0}\"", hashedContent);
            var eTag = new EntityTagHeaderValue(hashedEtag);
            return eTag;
        }

        //CacheControlHeaderValue cacheControl

        public static PlainTextResult Push(string content, bool chunked = false)
        {
            var response = new PlainTextResult(HttpStatusCode.OK, content)
            {
                Chunked = chunked
            };
            return response;
        }

        public static PlainTextResult Push(string content, CacheControlHeaderValue cacheControl, bool chunked = false)
        {
            var etag = CreateTag(content);
            var response = new PlainTextResult(HttpStatusCode.OK, content)
            {
                Chunked = chunked,
                ETag = etag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static PlainTextResult Push(HttpStatusCode status)
        {
            var response = new PlainTextResult(status);
            return response;
        }

        public static PlainTextResult Push(HttpStatusCode status, string content, bool chunked = false)
        {
            var response = new PlainTextResult(status, content)
            {
                Chunked = chunked
            };
            return response;
        }

        public static PlainTextResult Push(HttpStatusCode status, object content, bool chunked = false)
        {
            var value = content.ToString();
            var response = new PlainTextResult(status, value)
            {
                Chunked = chunked
            };
            return response;
        }

        public static PlainTextResult Push(object content, bool chunked = false)
        {
            var value = content.ToString();
            var response = new PlainTextResult(HttpStatusCode.OK, value)
            {
                Chunked = chunked
            };
            return response;
        }

        public static PlainTextResult Push(object content, CacheControlHeaderValue cacheControl, bool chunked = false)
        {
            var value = content.ToString();
            var etag = CreateTag(value);
            var response = new PlainTextResult(HttpStatusCode.OK, value)
            {
                Chunked = chunked,
                ETag = etag,
                CacheControlHeader = cacheControl
            };
            return response;
        }
    }
}