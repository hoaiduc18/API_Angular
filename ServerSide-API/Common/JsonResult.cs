﻿
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServerSide_API.Common
{
    public class JsonResult : IHttpActionResult
    {
        public JsonResult(HttpStatusCode statusCode, BaseResponse content)
        {
            StatusCode = statusCode;
            Content = content;
        }

        public JsonResult(HttpStatusCode statusCode, BaseViewModel content)
        {
            StatusCode = statusCode;
            Content = content;
        }

        public JsonResult(HttpStatusCode statusCode, FunctionResult content)
        {
            StatusCode = statusCode;
            Content = content;
        }

        public JsonResult(HttpStatusCode statusCode, IEnumerable<BaseViewModel> content)
        {
            StatusCode = statusCode;
            Content = content;
        }

        public JsonResult(HttpStatusCode statusCode, IEnumerable<BaseResponse> content)
        {
            StatusCode = statusCode;
            Content = content;
        }

        public JsonResult()
        {
            StatusCode = HttpStatusCode.OK;
            Content = null;
            Chunked = false;
        }

        public JsonResult(BaseResponse content)
            : this()
        {
            Content = content;
            StatusCode = HttpStatusCode.OK;
        }

        public JsonResult(BaseViewModel content)
            : this()
        {
            Content = content;
            StatusCode = HttpStatusCode.OK;
        }

        public JsonResult(HttpStatusCode statusCode)
            : this()
        {
            StatusCode = statusCode;
        }

        private static EntityTagHeaderValue CreateTag(BaseResponse content)
        {
            var hashedContent = content.GetHash();
            var hashedEtag = string.Format("\"{0}\"", hashedContent);
            var eTag = new EntityTagHeaderValue(hashedEtag);
            return eTag;
        }

        private static EntityTagHeaderValue CreateTag(BaseViewModel content)
        {
            var hashedContent = content.GetHash();
            var hashedEtag = string.Format("\"{0}\"", hashedContent);
            var eTag = new EntityTagHeaderValue(hashedEtag);
            return eTag;
        }

        private static EntityTagHeaderValue CreateTag(IEnumerable<BaseViewModel> content)
        {
            var hashedContent = new StringBuilder();
            foreach (var item in content)
            {
                var hash = item.GetHash();
                hashedContent.Append(hash);
            }

            var hashedEtag = string.Format("\"{0}\"", hashedContent);
            var eTag = new EntityTagHeaderValue(hashedEtag);
            return eTag;
        }

        private static EntityTagHeaderValue CreateTag(IEnumerable<BaseResponse> content)
        {
            var hashedContent = new StringBuilder();
            foreach (var item in content)
            {
                var hash = item.GetHash();
                hashedContent.Append(hash);
            }

            var hashedEtag = string.Format("\"{0}\"", hashedContent);
            var eTag = new EntityTagHeaderValue(hashedEtag);
            return eTag;
        }

        private static CacheControlHeaderValue CreateDefaultCacheHeader()
        {
            var cacheControlHeader = new CacheControlHeaderValue();
            cacheControlHeader.MaxAge = new TimeSpan(0, 10, 0);
            return cacheControlHeader;
        }

        public HttpStatusCode StatusCode { get; set; }

        public object Content { get; set; }

        public bool Chunked { get; set; }

        public CacheControlHeaderValue CacheControlHeader { get; set; }

        public EntityTagHeaderValue ETag { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                HttpResponseMessage response;

                var formatJson = new JsonMediaTypeFormatter
                {
                    SerializerSettings =
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }
                };
                response = new HttpResponseMessage
                {
                    Content = new ObjectContent<object>(Content, formatJson),
                    StatusCode = StatusCode,
                };
                response.Headers.TransferEncodingChunked = Chunked;
                response.Headers.CacheControl = CacheControlHeader;
                response.Headers.ETag = ETag;
                return response;
            }, cancellationToken);
        }

        public static JsonResult Push(BaseResponse content, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var cacheControl = CreateDefaultCacheHeader();
            var response = new JsonResult(HttpStatusCode.OK, content)
            {
                Chunked = chunked,
                ETag = eTag,
                //CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(BaseViewModel content, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var cacheControl = CreateDefaultCacheHeader();
            var response = new JsonResult(HttpStatusCode.OK, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(IEnumerable<BaseViewModel> content, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var cacheControl = CreateDefaultCacheHeader();
            var response = new JsonResult(HttpStatusCode.OK, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, BaseResponse content, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var cacheControl = CreateDefaultCacheHeader();
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, BaseViewModel content, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var cacheControl = CreateDefaultCacheHeader();
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, IEnumerable<BaseViewModel> content, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var cacheControl = CreateDefaultCacheHeader();
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, IEnumerable<BaseResponse> content, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var cacheControl = CreateDefaultCacheHeader();
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, BaseResponse content, CacheControlHeaderValue cacheControl, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, BaseViewModel content, CacheControlHeaderValue cacheControl, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, IEnumerable<BaseViewModel> content, CacheControlHeaderValue cacheControl, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(HttpStatusCode status, IEnumerable<BaseResponse> content, CacheControlHeaderValue cacheControl, bool chunked = false)
        {
            var eTag = CreateTag(content);
            var response = new JsonResult(status, content)
            {
                Chunked = chunked,
                ETag = eTag,
                CacheControlHeader = cacheControl
            };
            return response;
        }

        public static JsonResult Push(FunctionResult functionResult)
        {
            var status = functionResult.Succeed ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
            var response = new JsonResult(status, functionResult);
            return response;
        }
    }
}