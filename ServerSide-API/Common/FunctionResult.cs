﻿using System.Collections.Generic;

namespace Cadena.WebApi.Common
{
    public class FunctionResult
    {
        private List<string> _listErrors;

        public object Data { get; set; }

        public FunctionResult()
        {
            _listErrors = new List<string>();
        }

        public FunctionResult(string error)
            : this()
        {
            _listErrors.Add(error);
        }

        public FunctionResult(IEnumerable<string> listErrors)
            : this()
        {
            _listErrors.AddRange(listErrors);
        }

        public static FunctionResult Success = new FunctionResult();//=> private new FunctionResult();

        public bool Succeed { get; set; } //=> _listErrors.Count == 0;

        public bool Failed { get; set; }//=> _listErrors.Count > 0;

        public IEnumerable<string> Errors { get; set; }//=> _listErrors;

        public FunctionResult AddError(string error)
        {
            _listErrors.Add(error);
            return this;
        }

        public FunctionResult AddErrors(FunctionResult functionResult)
        {
            _listErrors.AddRange(functionResult.Errors);
            return this;
        }

        public static FunctionResult Error(string error)
        {
            var result = Success;
            result.AddError(error);
            return result;
        }

        public static FunctionResult Error(params string[] listErrors)
        {
            var result = Success;
            foreach (var error in listErrors)
                result.AddError(error);
            return result;
        }

        public void SetData(object data)
        {
            Data = data;
        }
    }
}