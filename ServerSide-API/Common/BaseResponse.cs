﻿
using Newtonsoft.Json;

namespace ServerSide_API.Common
{
    public abstract class BaseResponse
    {
        public override string ToString()
        {
            var json = JsonConvert.SerializeObject(this);
            return json;
        }

        public string GetHash()
        {
            var source = ToString();
            return source;
        }

    }
}
